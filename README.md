# ratp-fraternite-serveur
Web app working with [alerte-o-bus](https://github.com/UzfulLab/ratp-fraternite-arduinoYun) object.

###Dependencies :
- [MeteorJS](https://www.meteor.com/install)

Launch
```shell
meteor
```
to run the project.

Sometimes the stop_area ID change, so request result to a 404. When it happens, use postman or whatever http request simulator and find the new id this way :
* make this request :
```shell
http://api.navitia.io/v1/coverage/fr-idf/lines/line:OIF:100100102:102OIF442/stop_areas
```
to display all stop areas of the line 102 direction Gambetta. Then search for the name of stop your are looking for, in our case it's : Fraternité. Then copy and paste the appropriate stop_area value.


102, arrêt fraternité, direction rosny bois perrier
https://api.navitia.io/v1/coverage/fr-idf/lines/line:OIF:100100102:102OIF442/stop_areas/stop_area:OIF:SA:59:5231630/departures?from_datetime=20160104T154917

102, arrêt fraternité, direction rosny gambetta
https://api.navitia.io/v1/coverage/fr-idf/lines/line:OIF:100100102:102OIF442/stop_areas/stop_area:OIF:SA:59:4036622/departures?from_datetime=20160104T155600
