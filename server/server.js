var API_URL = 'https://api.navitia.io/v1/coverage/fr-idf/lines/line:OIF:100100102:102OIF442/stop_areas/stop_area:OIF:SA:59:3687631/departures?from_datetime=';
// test ligne 61 line:OIF:100100061:61OIF442
// test arret eglise de pantin stop_area:OIF:SA:59344

// arret fraternité stop_area:OIF:SA:59:5231593
// stop_area:OIF:SA:59:3687631


var mDateFormat = 'YYYYMMDDTHHmmss';

var API_KEY = "c4IjB3DwPLA8ZXjhfRKO3C";
var IFTTT_URL_TO_EVENT = "https://maker.ifttt.com/trigger/BUS_102_GAMBETTA/with/key/c4IjB3DwPLA8ZXjhfRKO3C";

var NextBusTime;
var minimumWarningLapse = 4;

Meteor.publish("newScheduledB102Frat2Gamb", function () {
    return Bus102schedules.find();
});

function storeAndPushlishBusTime(nTime) {
    if (Bus102schedules.find().count() == 0)
        Bus102schedules.insert({
            time2arrival: nTime
        });
    else {

        Bus102schedules.update(Bus102schedules.findOne()._id, {
            time2arrival: nTime
        });
    }

}

Meteor.startup(function () {
    var client = mqtt.connect('mqtt://web2.uzful.fr');

    moment.locale('fr');

    var bus102Gambetta = function () {

        var now = moment().format(mDateFormat);
        console.log(now);
        console.log(API_URL + now);
        var result = HTTP.get((API_URL + now), {
            auth: '7cc354b1-e7a4-4d8a-b735-669ae80289ff:'
        });

        var nextDeparture = result.data.departures[0].stop_date_time.arrival_date_time;

        console.log("Prochain départ d'un bus, en direction de Gambetta, à l'arrêt 'Fraternité'  : ", nextDeparture);

        if (!nextDeparture || nextDeparture == "") return;

        nextDeparture = moment(nextDeparture, mDateFormat);

        // a voir si afficher cette différence de temps est utile
        console.log(moment(nextDeparture, mDateFormat).fromNow());

        var diffMinutes = nextDeparture.diff(moment(), 'minutes');

        client.publish('uzfultopic', ""+diffMinutes);

        // différence de temps en nombre de minutes
        console.log("diff min:", diffMinutes);

        // le bus arrive, on déclenche la recette
        if (Number(diffMinutes) === minimumWarningLapse) {
            console.log("le bus arrive dans moins de 5 minutes !");
            var result = HTTP.get((IFTTT_URL_TO_EVENT));

        }

        NextBusTime = Number(diffMinutes);

        storeAndPushlishBusTime(NextBusTime);

        //            if (Number(diffMinutes) === 3) {
        //                console.log("le bus arrive dans moins de 5 minutes !");
        //                var result = HTTP.get((IFTTT_URL_TO_EVENT));
        //            }
    }

    var cron = new Meteor.Cron({
        events: {
            "* * * * *": bus102Gambetta
                //                "1,6,11,16,21,26,31,36,41,46,51,56 * * * *": bus102Gambetta
                //                "1,6,11,16,21,26,31,36,41,46,51,56 18-23 * * *": bus102Gambetta
        }
    });
});
